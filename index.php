<?php
    $sections = CIBlockSection::GetList(
        array('NAME' => 'ASC'),
        array('IBLOCK_ID' => 21),
        false,
        array('ID', 'NAME', 'CODE')
    );
    $section_abs = array();
    while ($section = $sections->GetNext()) {
        $section_abs[] = $section;
    }
    $arrayFirstLetter = '';
    foreach ($section_abs as $arSectionArray) {
        $sectFName = $arSectionArray['NAME'];
        $sectFLetter = mb_strtolower(mb_substr($sectFName, 0, 1, 'UTF-8'));
        if (!in_array($sectFLetter, array(' ', 'c', 'i', 'p', 'u', 'w', '3'))) { // пропускаем разделы, начинающиеся на буквы W, S, Z, G
            $arrayFirstLetter .= '\'' . $sectFLetter . '\',';
        }
    }
    eval('$FirstLetter=array(' . $arrayFirstLetter . ');');
    extract($FirstLetter);
    $uniqueLetter = array_unique($FirstLetter);
?>
<div class="catalog-list">
    <div class="catalog-item">
        <?php foreach ($uniqueLetter as $onlyOnceLetter) { ?>
            <div class="catalog-item__box">
                <?php echo '<div class="catalog-item__letter">' . strtoupper($onlyOnceLetter) . '</div>';?>
                <div class="catalog-item__box-list">
                    <ul>
                        <?php foreach ($section_abs as $arSection) {
                            $sectFName = $arSection['NAME'];
                            $sectFLetter = mb_strtolower(mb_substr($sectFName, 0, 1, 'UTF-8'));
                            if ($sectFLetter == $onlyOnceLetter && !in_array($sectFLetter, array(' ', 'c', 'i', 'p', 'u', 'w', '3'))) { ?>
                                <li class="catalog-item__box-list-element">
                                    <a href="/shop/<?=$arSection['CODE']?>">
                                        <?= $arSection['NAME']; ?>
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        <?php }?>
    </div>
</div>
